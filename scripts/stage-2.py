#!/usr/bin/python3
import os
from sys import platform
import subprocess
import shutil
import json
from xml.etree import ElementTree
import textwrap
import utils

utils.load_dotenv()
script_dir, prog_dir, ext_dir, working_dir = utils.get_paths()
moodle_data = utils.read_moodle_data()

with open("plagiarisms.txt", "r") as plag_txt:
    plagiarisms = plag_txt.read().split("\n")

for submission in moodle_data["submissions"]:
    if submission["valid"] and submission["folderName"] in plagiarisms:
        utils.declare_invalid(submission, "plagiarism")

utils.save_moodle_data(moodle_data)


os.chdir("submissions")
print(":: Starting unit tests")
null_file = open(os.devnull, "w")
for submission in moodle_data["submissions"]:
    if submission["valid"] == False:
        continue

    sub_folder = submission["folderName"]
    print(f"Testing {sub_folder}")

    os.chdir(sub_folder)
    for sub_file in os.listdir("."):
        if sub_file != "src":
            if os.path.isdir(sub_file):
                shutil.rmtree(sub_file)
            else:
                os.remove(sub_file)
    os.chdir("..")

    os.system(f"java -jar {os.path.join(ext_dir, 'codecopy.jar')} {os.path.join('..', 'test_submission')} {os.path.join(sub_folder, 'src')} {sub_folder}")
    
    os.chdir(os.path.join(sub_folder, "test_submission"))
    gradle_cmd = "gradlew.bat"
    if platform == "linux" or platform == "linux2" or platform == "darwin":
        os.system("chmod +x gradlew")
        gradle_cmd = "./gradlew"
    subprocess.call(f"{gradle_cmd} test", shell=True, stdout=null_file, stderr=null_file)
    os.chdir("..")
    os.chdir("..")
null_file.close()


print(":: Aggregating JUnit results")
os.chdir("..")
for submission in moodle_data["submissions"]:
    if not submission["valid"]:
        continue

    submission_report_dir = os.path.join("submissions", submission["folderName"], "test_submission", "build", "test-results", "test")
    if not os.path.isdir(submission_report_dir):
        utils.declare_invalid(submission, "system_error_no_test_results")
        continue
    submission["testSuites"] = list()
    for f in os.listdir(submission_report_dir):
        if f.startswith("TEST-") and f.endswith(".xml"):
            test_res_xml = ElementTree.parse(os.path.join(submission_report_dir, f)).getroot()
            test_res = {
                "name": test_res_xml.attrib["name"],
                "testCases": list()
            }
            submission["testSuites"].append(test_res)
            for test_case_xml in test_res_xml.findall("testcase"):
                test_case = {
                    "name": test_case_xml.attrib["name"],
                    "time": float(test_case_xml.attrib["time"]),
                    "passed": True
                }
                test_res["testCases"].append(test_case)
                failures = test_case_xml.findall("failure")
                if len(failures) > 0:
                    failure_xml = failures[0]
                    test_case["passed"] = False
                    test_case["error"] = failure_xml.attrib["type"]
                    test_case["errorMessage"] = failure_xml.attrib["message"]
                    test_case["stacktrace"] = failure_xml.text

utils.save_moodle_data(moodle_data)


print(":: Generating reports")
os.mkdir("email")
for submission in moodle_data["submissions"]:
    with open(os.path.join("email", submission["email"]), "a") as email_file:
        email_file.write(textwrap.dedent(f"""
        From: {os.environ['AUTOGRADER_EMAIL_FROM_NAME']} <{os.environ['AUTOGRADER_EMAIL_FROM_ADDRESS']}>
        To: {submission['name']} <{submission['email']}>
        Subject: {moodle_data['assignmentName']} - Automatische Bewertung

        {moodle_data['assignmentName']} - Automatische Bewertung

        Aufgaben-ID: {moodle_data['assignmentID']}
        Aufgabe: {moodle_data['assignmentName']}
        Schüler: {submission['name']}
        """))

        if not submission["valid"]:
            if submission["invalidReason"] == "no_submission":
                email_file.write("\nKeine Abgabe\n")
            elif submission["invalidReason"].startswith("system_error"):
                email_file.write(textwrap.dedent(f"""
                Während der Bewertung ist ein Fehler aufgetreten.
                Bitte kontaktiere den Kursleiter.

                Fehlercode: {submission['invalidReason']}
                """))
            else:
                email_file.write("\nDeine Abgabe ist ungültig. (Grund: ")
                email_file.write({
                    "invalid_file_type": "falscher Dateityp",
                    "invalid_folder_structure": "ungültige Dateistruktur",
                    "plagiarism": "als Plagiat markiert"
                }.get(submission["invalidReason"], submission["invalidReason"]))
                email_file.write(")\n")
        else:
            email_file.write("\nTestergebnisse:\n")
            for test_suite in submission["testSuites"]:
                email_file.write(f"{test_suite['name']}:\n")
                for test_case in test_suite['testCases']:
                    email_file.write(f"  {test_case['name']}: ")
                    if test_case["passed"]:
                        email_file.write("passed\n")
                    else:
                        email_file.write(f"failed\n    {test_case['errorMessage']}\n")
