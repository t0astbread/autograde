#!/usr/bin/python3
import os
import subprocess
import shutil
import zipfile
import json
import utils

utils.load_dotenv()
script_dir, prog_dir, ext_dir, working_dir = utils.get_paths()

print(":: Downloading submissions")
if "AUTOGRADE_ENV" not in os.environ:
    os.environ["AUTOGRADE_ENV"] = "prod"

if os.environ["AUTOGRADE_ENV"] != "dev":
    os.environ["NODE_ENV"] = "dev"

    os.chdir(os.path.join(ext_dir, "moodle_data_grabber"))
    subprocess.check_call("npm run start-prod", shell=True, env=os.environ)
    os.chdir(working_dir)

shutil.copy(os.path.join(ext_dir, "moodle_data_grabber", "downloads", "submissions.zip"), "submissions.zip")
shutil.copy(os.path.join(ext_dir, "moodle_data_grabber", "downloads", "submissions.json"), "submissions.json")

if os.environ["AUTOGRADE_ENV"] != "dev":
    shutil.rmtree(os.path.join(ext_dir, "moodle_data_grabber", "downloads"))
    shutil.rmtree(os.path.join(ext_dir, "moodle_data_grabber", "browserDataDir"))

submissions_zip = zipfile.ZipFile("submissions.zip", "r")
os.mkdir("submissions_stage_1")
submissions_zip.extractall(path="submissions_stage_1")
submissions_zip.close()

os.mkdir("submissions")
for submission_dir in os.listdir("submissions_stage_1"):
    submission_dir_files = os.listdir(os.path.join("submissions_stage_1", submission_dir))
    if len(submission_dir_files) == 1 and submission_dir_files[0].endswith(".zip"):
        submission_zip = zipfile.ZipFile(os.path.join("submissions_stage_1", submission_dir, submission_dir_files[0]), "r")
        submission_zip.extractall(path="submissions")
        submission_zip.close()
shutil.rmtree("submissions_stage_1")
shutil.rmtree(os.path.join("submissions", "__MACOSX"))

moodle_data = utils.read_moodle_data()

with open("teachers.txt", "r") as teachers_txt:
    teachers = teachers_txt.read().split("\n")
moodle_data["submissions"] = [sub for sub in moodle_data["submissions"] if sub["name"] not in teachers]

os.mkdir("invalid_submissions")
for i, submission in enumerate(moodle_data["submissions"]):
    submission["valid"] = True

    if submission["fileName"] is None:
        utils.declare_invalid(submission, "no_submission")
        continue
    elif not submission["fileName"].endswith(".zip"):
        utils.declare_invalid(submission, "invalid_file_type")
        continue
    submission["folderName"] = submission["fileName"][:-4]
    if "src" not in os.listdir(os.path.join("submissions", submission["folderName"])):
        utils.declare_invalid(submission, "invalid_folder_structure")

print(":: Cleaning submissions")
os.chdir("submissions")
for submission in moodle_data["submissions"]:
    if not submission["valid"]:
        continue

    sub_folder = submission["folderName"]

    os.chdir(sub_folder)
    for sub_file in os.listdir("."):
        if sub_file != "src":
            if os.path.isdir(sub_file):
                shutil.rmtree(sub_file)
            else:
                os.remove(sub_file)
    os.chdir("..")

print(":: Starting JPlag")
os.system(f"java -jar {os.path.join(ext_dir, 'jplag.jar')} -s -S src -l java17 -r {os.path.join('..', 'jplag_results')} .")

os.chdir("..")
utils.save_moodle_data(moodle_data)
