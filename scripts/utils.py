import os
import shutil
import json
import dotenv

def load_dotenv():
    dotenv.load_dotenv("./.env")

def get_paths():
    script_dir = os.path.dirname(os.path.abspath(__file__))
    prog_dir = os.path.abspath(os.path.join(script_dir, os.pardir))
    ext_dir = os.path.join(prog_dir, "ext")
    working_dir = os.getcwd()
    return (script_dir, prog_dir, ext_dir, working_dir)

def read_moodle_data():
    with open("submissions.json", "r") as moodle_data_file:
        moodle_data = json.load(moodle_data_file)
    return moodle_data

def save_moodle_data(moodle_data):
    with open("submissions.json", "w") as moodle_data_file:
        json.dump(moodle_data, moodle_data_file)

def declare_invalid(submission, reason):
    submission["valid"] = False
    submission["invalidReason"] = reason
    if "folderName" in submission:
        shutil.move(os.path.join("submissions", submission["folderName"]), "invalid_submissions")
