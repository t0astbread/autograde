#!/usr/bin/python3
import os
import smtplib
from email.mime.text import MIMEText
import utils

utils.load_dotenv()
is_dev_env = os.environ.get("AUTOGRADER_ENV", "prod") == "dev"

smtp_host = os.environ["AUTOGRADER_SMTP_HOST"]
smtp_port = int(os.environ.get("AUTOGRADER_SMTP_PORT", smtplib.SMTP_PORT if is_dev_env else smtplib.SMTP_SSL_PORT))

if is_dev_env:
    smtp = smtplib.SMTP(smtp_host, smtp_port)
else:
    smtp = smtplib.SMTP_SSL(smtp_host, smtp_port)
    smtp.login(os.environ["AUTOGRADER_SMTP_USERNAME"], os.environ["AUTOGRADER_SMTP_PASSWORD"])

print(":: Sending report email")
for recipient in os.listdir("email"):
    with open(os.path.join("email", recipient), "r") as email_file:
        mailfile_text = email_file.read()
        mailfile_lines = mailfile_text.split("\n")[1:]
        for line in mailfile_lines:
            if len(line) == 0:
                break
            if line.startswith("From: "):
                mail_from = line[6:]
            elif line.startswith("To: "):
                mail_to = line[4:]
            elif line.startswith("Subject: "):
                mail_subject = line[9:]
        mailfile_text = "\n".join(mailfile_lines[4:])
        message = MIMEText(mailfile_text.encode("utf-8"), _charset="utf-8")
        message["From"] = mail_from
        message["To"] = mail_to
        message["Subject"] = mail_subject
    smtp.sendmail(os.environ["AUTOGRADER_EMAIL_FROM_ADDRESS"], recipient, message.as_string())

smtp.close()
