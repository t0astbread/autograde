# autograder

## Requirements
  - Python 3
  - `python-dotenv` package (`pip install python-dotenv`)
  - NodeJS (any recent LTS release)
  - npm (should come with NodeJS)
  - Java 11 JDK
  - SMTP access (optional, for email reports)

## Setup
After you've installed the above mentioned requirements, you must
execute the `setup.py` script. This script will set some dependencies
up inside the autograder tool. It does not modify any other files.

You only have to run this script once per installation.

## General usage
The program is divided into 3 parts (stages). These stages each
consist of a Python script located in `scripts/`. To execute a stage
simply execute the script (with Python 3).

You can execute the scripts in any working directory of your
choosing and the scripts will use it as their working directory.

After each stage, manual intervention is required. This typically
consists of writing a text file to pass some data to the next script.
(The scripts don't take command line arguments.)

Additionally, the scripts use environment variables. These can be
defined either as real environment variables or as `KEY=value`
entries in a file called `.env` in the working directory.

It is generally recommended to define all environment variables
upfront, however not every scripts needs all environment variables.

## Data requried for each stage
### Stage 1
Envrionment variables:
  - `MOODLE_USERNAME`
  - `MOODLE_PASSWORD`
  - `MOODLE_ASSIGNMENT_ID`
  - `MOODLE_BASE_URL`: The full URL of your Moodle's login page minus
     `/login/index.php`

Additional files:
  - `teachers.txt`: A list of all Moodle course participants who are
     teachers, referred to by their Moodle name

Output:
  - `jplag_results/`: The JPlag report

### Stage 2
Environment variables:
  - `AUTOGRADER_EMAIL_FROM_NAME`: The name to use in the "From"
     header of outgoing email
  - `AUTOGRADER_EMAIL_FROM_ADDRESS`: The email address to send email
     from (note that no email is sent yet)

Additional files:
  - `test_submission/`: The reference implementation used for testing
     submissions. Must be a Gradle project, can still contain sources,
     will not be modified
  - `plagiarisms.txt`: A list of all submissions marked as plagiats,
     referred to by their display name in the JPlag report

Output:
  - `submissions.json`: A machine-readable report of all tests
  - `email/`: The outgoing email to be sent (includes test results
     for each student)

### Stage 3
Environment variables:
  - `AUTOGRADER_SMTP_HOST`
  - `AUTOGRADER_SMTP_PORT`
  - `AUTOGRADER_SMTP_USERNAME`
  - `AUTOGRADER_SMTP_PASSWORD`

## Additional Notes
Sometimes the web scraper in stage 1 will hang. If that's the case,
simply clean the folder and restart it.

If you're unsure if your inputs for a stage are correct, back up the
working directory before you start the stage. Except for stage 3
(which doesn't change any files on disk), stages can usually not
recover from a corrupted working directory.
