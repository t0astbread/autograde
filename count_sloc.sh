#!/bin/sh
find . \( \
    -path ./.vscode/.ropeproject \
    -o -path ./ext/moodle_data_grabber/node_modules \
    -o -path ./ext/codecopy/build \
    -o -path ./debug/test_submission \
    -o -path ./debug/exec \
\) -prune \
-o \( \
    -name "*.py" \
    -o -name "index.mjs" \
    -o -name "*.sh" \
    -o -name "*.java" \
    -o -name "*.kt" \
\) -print \
| xargs cat | grep -cvE ^\s*$
