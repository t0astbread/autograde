package net.htlgrieskirchen.pos2.memos;

import org.junit.Assert;
import org.junit.Test;

public class TestMemo {
    @Test
    public void testNote() {
        Memo memo = new Memo();
        String note = "123abc";
        memo.setNote(note);
        Assert.assertEquals(note, memo.getNote());
    }

    @Test
    public void testNoteFail() {
        Memo memo = new Memo();
        String note = "123abc";
        memo.setNote(note);
        Assert.assertNotEquals(note, memo.getNote());
    }
}
