/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.htlgrieskirchen.pos2.memos;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import static net.htlgrieskirchen.pos2.memos.Main.RESOURCE_BUNDLE_NAME;


/**
 *
 * @author alexander
 */
public class Controller implements Initializable {
    
    private Label label;
    @FXML
    private Menu menu;
    @FXML
    private MenuItem menuItemSave;
    @FXML
    private MenuItem menuItemGerman;
    @FXML
    private MenuItem menuItemEnglish;
    @FXML
    private Label labelDate;
    @FXML
    private DatePicker datePicker;
    @FXML
    private Label labelCategory;
    @FXML
    private ComboBox<Category> comboBox;
    @FXML
    private TextField textField;
    @FXML
    private ListView<Memo> listView;
    @FXML
    private Button button;
    
    Model model;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            model = new Model();
            comboBox.setItems(FXCollections.observableArrayList(Category.values()));
            listView.setItems(FXCollections.observableArrayList(model.getMemos()));
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setContentText(new RuntimeException().getMessage());
        }
    }    

    @FXML
    private void handleMenuItemSave(ActionEvent event) {
         try{
            model.saveMemos();
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fehler");
            alert.setContentText(new RuntimeException().getMessage());
        }
    }

    @FXML
    private void handleMenuItemGerman(ActionEvent event) {
        changeLanguage(Locale.GERMAN);
    }

    @FXML
    private void handleMenuItemEnglish(ActionEvent event) {
        changeLanguage(Locale.ENGLISH);
    }

    @FXML
    private void handleButton(ActionEvent event) {
        Date date = Date.from(datePicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
        Category category = comboBox.getValue();
        String note = textField.getText();
        
        Memo memo = new Memo(date, category, note);
        
        model.addMemo(memo);
        
        listView.setItems(FXCollections.observableArrayList(model.getMemos()));
        
        datePicker.setValue(null);
        comboBox.setValue(null);
        textField.setText(null);
    }
    
    private Locale changeLanguage(Locale locale){
        ResourceBundle rb = ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME, locale);
        
        menu.setText(rb.getString("file"));
        menuItemSave.setText(rb.getString("save"));
        button.setText(rb.getString("add"));
        labelDate.setText(rb.getString("date"));
        labelCategory.setText(rb.getString("category"));
//        labelNote.setText(rb.getString("note"));
        
        Memo.changeLocale(locale);
        
        listView.setItems(FXCollections.observableArrayList(model.getMemos()));
        
        return locale;
    }
    
}
