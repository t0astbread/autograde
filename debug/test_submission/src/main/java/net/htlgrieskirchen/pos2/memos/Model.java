/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.htlgrieskirchen.pos2.memos;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author alexander
 */
public class Model {
    private File file = new File("net.htlgrieskirchen.pos2.memos.xml");
    private Set<Memo> memos = new TreeSet<>();

    public Model() {
    }
    
    
    public Memo addMemo(Memo memo){
        memos.add(memo);
        
        return memo;
    }
    
    public Set<Memo> getMemos(){
        return Collections.unmodifiableSet(memos);
    }
    
    public void loadMemos(){
        try (final XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(file)))) {
            memos = (Set<Memo>) decoder.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void saveMemos(){
        try (final XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(file)))) {
            encoder.writeObject(memos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
