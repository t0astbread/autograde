/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.htlgrieskirchen.pos2.memos;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author alexander
 */
public class Main extends Application {
    
    public static final String RESOURCE_BUNDLE_NAME = "net.htlgrieskirchen.pos2.net.htlgrieskirchen.pos2.memos.MessageBundle_de";
    private static Controller controller;
    
    @Override
    public void start(Stage stage) throws Exception {
        ResourceBundle rb = ResourceBundle.getBundle(RESOURCE_BUNDLE_NAME, Locale.getDefault());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("View.fxml"), rb);
        Parent root = loader.load();
        controller = loader.getController();
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public static Controller getController() {

        return controller;
    }
}
