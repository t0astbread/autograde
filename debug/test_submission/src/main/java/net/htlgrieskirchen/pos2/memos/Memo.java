/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.htlgrieskirchen.pos2.memos;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author alexander
 */
public class Memo implements Comparable<Memo> {
    private static DateFormat df;
    private Date date;
    private Category category;
    private String note;

    public Memo() {
    }

    public Memo(Date date, Category category, String note) {
        this.date = date;
        this.category = category;
        this.note = note;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDate() {
        return date;
    }

    public Category getCategory() {
        return category;
    }

    public String getNote() {
        return note;
    }

    public static Locale changeLocale(Locale locale) {
        df = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        return locale;
    }

    @Override
    public String toString() {
        return "[" + df.format(date) + "] " + category + ": " + note;
    }

    public int compareTo(Memo o) {
        if(this.date.compareTo(o.date) > 0) {
            return -1;
        }
        else {
            return 1;
        }
    }
}
