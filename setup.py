#!/usr/bin/python3
import os
from sys import platform
import shutil
from urllib.request import urlretrieve

os.chdir("ext/moodle_data_grabber")
os.system("npm install")

os.chdir("../codecopy")
if platform == "linux" or platform == "linux2" or platform == "darwin":
    os.system("chmod +x gradlew")
    os.system("./gradlew build")
else:
    os.system("gradlew.bat build")
shutil.copy("build/libs/codecopy-1.0-SNAPSHOT.jar", "../codecopy.jar")
os.chdir("..")

urlretrieve("https://github.com/jplag/jplag/releases/download/v2.11.9-SNAPSHOT/jplag-2.11.9-SNAPSHOT-jar-with-dependencies.jar", filename="jplag.jar")
